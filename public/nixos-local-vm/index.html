<!DOCTYPE html>
<html lang="en">
    <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <title>Dejà vu - Updating NixOS local VMs</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="alternate" type="application/rss+xml" title="RSS" href="https://blog.patapon.info/rss.xml">
      
      <link rel="stylesheet" href="https://blog.patapon.info/site.css">
      
    </head>
    <body>
      <header>
        <h1><a href="https:&#x2F;&#x2F;blog.patapon.info">Dejà vu</a></h1>
        <nav>
        
          <a href="https:&#x2F;&#x2F;blog.patapon.info">Home</a>
           | 
        
          <a href="https:&#x2F;&#x2F;blog.patapon.info&#x2F;tags">Tags</a>
          
        
        </nav>
      </header>
      <main>
      
<article>
    
<h1>Updating NixOS local VMs</h1>
<p class="article-metadata">
  <small>
    <time>Posted on 2019-12-01</time> by eon@patapon.info<span class="tags">,
      Tags:
      
        <a href="https://blog.patapon.info/tags/nix/">#nix</a>
      
        <a href="https://blog.patapon.info/tags/nixos/">#nixos</a>
      
    </span>
    
  </small>
</p>

    <p>So you have deployed a NixOS server and want to test a configuration change.
How cool would it be to test it locally before sending it over the wire and
hope for the best? Well NixOS provides an easy way to start a local VM using
your server configuration...</p>
<span id="continue-reading"></span><h1 id="intro">Intro</h1>
<p>This post will demonstrate how to build and run a VM from a NixOS configuration
and then update the configuration of the running VM on the fly.</p>
<p>This can be useful for different use-cases:</p>
<ul>
<li>testing the migration to a new config</li>
<li>testing the upgrade to a newer nixpkgs version</li>
<li>iterate faster when developping a NixOS module</li>
</ul>
<p>The work machine doesn't need to be NixOS, it can be any system with nix
installed.</p>
<h1 id="building-a-local-vm">Building a local VM</h1>
<p>Let's say you have your server <code>configuration.nix</code> file at hand and you want to
use that to build a local vm.</p>
<p>Let's take a simple example with this file:</p>
<pre data-lang="nix" class="language-nix "><code class="language-nix" data-lang="nix">{ config, pkgs, lib, ... }:

{
  imports = [
    .&#x2F;hardware-configuration.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = &quot;&#x2F;dev&#x2F;vda&quot;;

  networking.hostName = &quot;fripon&quot;;
  networking.domain = &quot;patapon.info&quot;;

  i18n.defaultLocale = &quot;en_US.UTF-8&quot;;
  time.timeZone = &quot;Europe&#x2F;Paris&quot;;

  services.openssh.enable = true;
  services.openssh.permitRootLogin = &quot;yes&quot;;
  services.timesyncd.enable = true;

  environment.systemPackages = with pkgs; [
    vim htop dnsutils inetutils
  ];

  users.mutableUsers = false;
  users.users.root.hashedPassword =
    &quot;$6$IJFASoJI$7x650JQGObqKBxPhxXhmegiWED.XUmolNfwHQW1jf.2NGvWQ7uF6yh2A5Sq67Lkj.9twhoCSZkoMFqDEnDN2R.&quot;;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = &quot;18.09&quot;; # Did you read the comment?
}
</code></pre>
<p>Nothing fancy, openssh, a root password set, locales, hostname etc...</p>
<p>We also need the <code>./hardware-configuration.nix</code> file from the server.</p>
<p>Next we can try to build a VM using this configuration using <code>nixos-rebuild</code>
(if you don't have <code>nixos-rebuild</code> command read the next section):</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIXOS_CONFIG=$(pwd)&#x2F;configuration.nix nixos-rebuild build-vm
building Nix...
building the system configuration...
error: The option `services.timesyncd.enable&#x27; has conflicting definitions, in `&#x2F;nix&#x2F;var&#x2F;nix&#x2F;profiles&#x2F;per-user&#x2F;root&#x2F;channels&#x2F;nixpkgs&#x2F;nixos&#x2F;modules&#x2F;virtualisation&#x2F;qemu-vm.nix&#x27; and `configuration.nix&#x27;.
(use &#x27;--show-trace&#x27; to show detailed location information)
</code></pre>
<p>Ok that didn't go well.</p>
<p>So we used the <code>nixos-rebuild build-vm</code> command and set the <code>NIXOS_CONFIG</code> env
variable to feed our configuration to <code>nixos-rebuild</code>. By default
<code>nixos-rebuild</code> will use <code>/etc/nixos/configuration.nix</code>.</p>
<p>Error message says that there is a conflicting option between our configuration
and <code>/nix/var/nix/profiles/per-user/root/channels/nixpkgs/nixos/modules/virtualisation/qemu-vm.nix</code>.
But wait, where does that come from?</p>
<p>The thing is <code>nixos-rebuild build-vm</code> does a magic trick which is to
automatically include this <code>qemu-vm.nix</code> module in our configuration to build
the local vm. If we look at <code>nixpkgs/nixos/default.nix</code> we see this:</p>
<pre data-lang="nix" class="language-nix "><code class="language-nix" data-lang="nix"># This is for `nixos-rebuild build-vm&#x27;.
vmConfig = (import .&#x2F;lib&#x2F;eval-config.nix {
  inherit system;
  modules = [ configuration .&#x2F;modules&#x2F;virtualisation&#x2F;qemu-vm.nix ];
}).config;
</code></pre>
<p>Ok so looking at <code>qemu-vm.nix</code> we can quickly find that this module wants to
disable <code>timesyncd</code>:</p>
<pre data-lang="nix" class="language-nix "><code class="language-nix" data-lang="nix"># Don&#x27;t run ntpd in the guest.  It should get the correct time from KVM.
services.timesyncd.enable = false;
</code></pre>
<p>So a quick and easy fix in our configuration would be to define:</p>
<pre data-lang="nix" class="language-nix "><code class="language-nix" data-lang="nix">services.timesyncd.enable = lib.mkDefault true;
</code></pre>
<p>With this the <code>qemu-vm.nix</code> module will be able to disable <code>timesyncd</code> but when
we will actually deploy this configuration to our server <code>timesyncd</code> will be
enabled.</p>
<p>The <code>qemu-vm.nix</code> module is interesting because it overrides things for us in order to
run our configuration in a VM. For example the disk layout, bootloader...</p>
<p>Let's try again!</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIXOS_CONFIG=$(pwd)&#x2F;configuration.nix nixos-rebuild build-vm
building Nix...
building the system configuration...
querying info about &#x27;&#x2F;nix&#x2F;store&#x2F;rqdzyjiq5xi4sz0c5pm9882g3bk4hg9s-nixos-vm&#x27; on &#x27;https:&#x2F;&#x2F;cache.nixos.org&#x27;...
downloading &#x27;https:&#x2F;&#x2F;cache.nixos.org&#x2F;rqdzyjiq5xi4sz0c5pm9882g3bk4hg9s.narinfo&#x27;...
querying info about &#x27;&#x2F;nix&#x2F;store&#x2F;rc0hnpn8vhl494xvrdniph2r0225qfcf-closure-info&#x27; on &#x27;https:&#x2F;&#x2F;cache.nixos.org&#x27;...
[...]
building &#x27;&#x2F;nix&#x2F;store&#x2F;rad4rzgrrmifprjw2mhmifvkk9gwiyyr-nixos-system-fripon-19.09pre-git.drv&#x27;...
building &#x27;&#x2F;nix&#x2F;store&#x2F;c6c8yhblcrsp36585im8aczaq65zb4ra-closure-info.drv&#x27;...
building &#x27;&#x2F;nix&#x2F;store&#x2F;mllcw7pviw6a29n18vmrgq3i1piax2c9-run-nixos-vm.drv&#x27;...
building &#x27;&#x2F;nix&#x2F;store&#x2F;ngmyx87fl7sqwlnmr9cwp2njjk3iwnji-nixos-vm.drv&#x27;...

Done.  The virtual machine can be started by running &#x2F;nix&#x2F;store&#x2F;rqdzyjiq5xi4sz0c5pm9882g3bk4hg9s-nixos-vm&#x2F;bin&#x2F;run-fripon-vm
</code></pre>
<p>Nice the VM configuration was built and the build output is a script that runs
the VM. We can run it easily with:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ .&#x2F;result&#x2F;bin&#x2F;run-fripon-vm
</code></pre>
<p>This will start the VM using <code>qemu</code>.</p>
<p>The first build might take some time because your <code>/nix/store</code> needs to be
populated with all the dependencies required by the configuration. The step
that actually create the disk image and run the VM is extremelly fast because
the <code>/nix/store</code> is shared between the host and the VM.</p>
<h2 id="accessing-the-vm-with-ssh">Accessing the VM with ssh</h2>
<p>We can pass <code>qemu</code> network options through <code>QEMU_NET_OPTS</code> env variable:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ QEMU_NET_OPTS=hostfwd=tcp::2221-:22 .&#x2F;result&#x2F;bin&#x2F;run-fripon-vm
$ ssh root@localhost -p 2221
</code></pre>
<p>This makes <code>qemu</code> listen on port <code>2221</code> and forward all connections to the port
<code>22</code> of the VM.</p>
<h2 id="building-without-nixos-rebuild">Building without <code>nixos-rebuild</code></h2>
<p>Actually <code>nixos-rebuild build-vm</code> doesn't do anything special. It's
just this same as building the <code>vm</code> attribute of <code>nixpkgs/nixos/default.nix</code>:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIXOS_CONFIG=$(pwd)&#x2F;configuration.nix nix-build &#x27;&lt;nixpkgs&#x2F;nixos&gt;&#x27; -A vm
</code></pre>
<p>Or:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ nix-build &#x27;&lt;nixpkgs&#x2F;nixos&gt;&#x27; -A vm --arg configuration .&#x2F;configuration.nix
</code></pre>
<h1 id="using-a-specific-version-of-nixpkgs">Using a specific version of nixpkgs</h1>
<p>So we are able to build a VM from a NixOs configuration but it's using <code>nixpkgs</code>
from our local host. Our server might be on a different release or commit of <code>nixpkgs</code>.</p>
<p>There is multiple ways to pin <code>nixpkgs</code> to a specific version. For now a simple way
would be to use a <code>nixpkgs</code> local git clone at a certain commit and use that for our
VM build by overriding <code>NIX_PATH</code>:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIX_PATH=nixpkgs=${HOME}&#x2F;vcs&#x2F;nixpkgs nix-build &#x27;&lt;nixpkgs&#x2F;nixos&gt;&#x27; -A vm --arg configuration .&#x2F;configuration.nix
</code></pre>
<h1 id="testing-only-the-build-of-the-config">Testing only the build of the config</h1>
<p>Since now we built a script that allows us to run our configuration in a VM.
The build of the VM configuration is a dependency of that script. The build
of a configuration results in a directory layout containing all system files
every time.</p>
<p>This is usually done using <code>nixos-rebuild build</code> which in fact is the same as
building the <code>system</code> attribute of <code>nixpkgs/nixos/default.nix</code>:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIX_PATH=nixpkgs=${HOME}&#x2F;vcs&#x2F;nixpkgs nix-build &#x27;&lt;nixpkgs&#x2F;nixos&gt;&#x27; -A system --arg configuration .&#x2F;configuration.nix
</code></pre>
<p>But in this case the config does not use the <code>qemu-vm.nix</code> module. What we want
is to build the configuration with the <code>qemu-vm.nix</code> module so that we can deploy
it on a running local VM.</p>
<h2 id="building-the-config-for-vm-use">Building the config for VM use</h2>
<p>To build the configuration for the VM we need to write a bit of nix in a <code>default.nix</code> file for example:</p>
<pre data-lang="nix" class="language-nix "><code class="language-nix" data-lang="nix">{ configuration
, system ? builtins.currentSystem
}:

let

  eval = modules: import &lt;nixpkgs&#x2F;nixos&#x2F;lib&#x2F;eval-config.nix&gt; {
    inherit system modules;
  };

in {

  vmSystem =
    (eval [ configuration &lt;nixpkgs&#x2F;nixos&#x2F;modules&#x2F;virtualisation&#x2F;qemu-vm.nix&gt; ]).config.system.build.toplevel;

}
</code></pre>
<p>First we define an <code>eval</code> function which takes a list of NixOS modules and calls
<code>nixpkgs/nixos/lib/eval-config.nix</code> to evaluate them.</p>
<p>Next, we expose a <code>vmSystem</code> attribute that uses <code>eval</code> with our system
configuration and the <code>qemu-vm.nix</code> module. We return the
<code>config.system.build.toplevel</code> attribute of the resulting evaluation.</p>
<p>This attribute give us the root of the system layout:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">$ NIX_PATH=nixpkgs=${HOME}&#x2F;vcs&#x2F;nixpkgs nix-build -A vmSystem --arg configuration .&#x2F;configuration.nix
[...]
&#x2F;nix&#x2F;store&#x2F;kr13nc1725pmm6biwbjr9yr7rjy7hahm-nixos-system-fripon-19.09.git.3ad23e3
$ ls -l &#x2F;nix&#x2F;store&#x2F;kr13nc1725pmm6biwbjr9yr7rjy7hahm-nixos-system-fripon-19.09.git.3ad23e3
.r-xr-xr-x  13k root  1 Jan  1970 activate
lrwxrwxrwx   91 root  1 Jan  1970 append-initrd-secrets -&gt; &#x2F;nix&#x2F;store&#x2F;2dggzxanjra05bljab2wgl1wg5s9v5a2-append-initrd-secrets&#x2F;bin&#x2F;append-initrd-secrets
dr-xr-xr-x    - root  1 Jan  1970 bin
.r--r--r--    0 root  1 Jan  1970 configuration-name
lrwxrwxrwx   51 root  1 Jan  1970 etc -&gt; &#x2F;nix&#x2F;store&#x2F;mldhlrvyldvdj7lg66yzsbzim3y59anq-etc&#x2F;etc
.r--r--r--    0 root  1 Jan  1970 extra-dependencies
dr-xr-xr-x    - root  1 Jan  1970 fine-tune
lrwxrwxrwx   65 root  1 Jan  1970 firmware -&gt; &#x2F;nix&#x2F;store&#x2F;1lq1wqmzr5ap66yhnl3lv67py1sm88d3-firmware&#x2F;lib&#x2F;firmware
.r-xr-xr-x 5.4k root  1 Jan  1970 init
.r--r--r--    9 root  1 Jan  1970 init-interface-version
lrwxrwxrwx   71 root  1 Jan  1970 initrd -&gt; &#x2F;nix&#x2F;store&#x2F;mrx22ach3pgn0ic7wnnk33836smdy9ld-initrd-linux-4.19.81&#x2F;initrd
lrwxrwxrwx   65 root  1 Jan  1970 kernel -&gt; &#x2F;nix&#x2F;store&#x2F;gvg17g25nr4jbq2pc26107yiqvk3m0d8-linux-4.19.81&#x2F;bzImage
lrwxrwxrwx   58 root  1 Jan  1970 kernel-modules -&gt; &#x2F;nix&#x2F;store&#x2F;0bryzvzfy0s4yh53zkzl4rrn30c5a8nj-kernel-modules
.r--r--r--   24 root  1 Jan  1970 kernel-params
.r--r--r--   17 root  1 Jan  1970 nixos-version
lrwxrwxrwx   55 root  1 Jan  1970 sw -&gt; &#x2F;nix&#x2F;store&#x2F;xvk85q1ymllz43k657c2a7248qbvx4wd-system-path
.r--r--r--   12 root  1 Jan  1970 system
lrwxrwxrwx   55 root  1 Jan  1970 systemd -&gt; &#x2F;nix&#x2F;store&#x2F;7yypimpzkxjh5dm2aajdx4051l1xlw72-systemd-243
</code></pre>
<p>At this point it's possible to check the build of a particular configuration
file without running the VM.</p>
<p>Also we can imagine injecting our own modules specific to the VM configuration
to the <code>eval</code> function if necessary.</p>
<h1 id="updating-a-running-local-vm-with-a-new-config">Updating a running local VM with a new config</h1>
<p>So we know how to build and run a local VM. We have also a way to build the
configuration with the <code>qemu-vm.nix</code> module. Now it would be cool to be able
to build a new configuration locally and if that works activate it in the running
VM.</p>
<p>Usually to update a running NixOS system the <code>nixos-rebuild switch</code> is used. It
looks by default for a configuration at <code>/etc/nixos/configuration.nix</code>, build
it and activate it on the host. Behind the scenes it mainly runs 3 commands:</p>
<ol>
<li><code>system=$(nix-build '&lt;nixpkgs/nixos&gt;' -A system)</code></li>
<li><code>nix-env -p /nix/var/nix/profiles/system --set $system</code></li>
<li><code>$system/bin/switch-to-configuration switch</code></li>
</ol>
<p>The activation of the new system is done by the <code>switch-to-configuration</code> script.
This script takes care of migrating the state of the current system according to
the new configuration. For example: start new services, unmount filesystems,
updating the bootloader...</p>
<p>We need another piece to copy the new system to the running VM: <code>nix-copy-closure</code>:</p>
<pre><code>NAME
       nix-copy-closure - copy a closure to or from a remote machine via SSH

SYNOPSIS
       nix-copy-closure [--to | --from] [--gzip] [--include-outputs] [--use-substitutes | -s] [-v] user@machine
                        paths
</code></pre>
<p><code>nix-copy-closure</code> is used to copy nix stores paths between nix enabled machines.
It is quite efficient because if some path is already present on the target
machine it won't be uploaded. It can also use substitutes meaning that if some
path yout want to upload is present in a binary cache it will be pulled from
it. This is very useful when your bandwidth is limited.</p>
<p>So given a local VM running and accessible via ssh on port <code>2221</code> we can deploy
a new configuration with a little script. Let's name it <code>deploy.sh</code>:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">#!&#x2F;usr&#x2F;bin&#x2F;env bash

set -e

NIX_PATH=nixpkgs=${HOME}&#x2F;vcs&#x2F;nixpkgs
PROFILE=&#x2F;nix&#x2F;var&#x2F;nix&#x2F;profiles&#x2F;system

# Build the VM system
outPath=$(nix-build -A vmSystem --arg configuration .&#x2F;configuration.nix)
# Upload to the VM
NIX_SSHOPTS=&quot;-p 2221&quot; nix-copy-closure --to &quot;root@localhost&quot; --gzip $outPath
# Activate the new system
ssh -p 2221 root@localhost nix-env --profile &quot;$PROFILE&quot; --set &quot;$outPath&quot;
ssh -p 2221 root@localhost $outPath&#x2F;bin&#x2F;switch-to-configuration test
</code></pre>
<p>Our script uses <code>switch-to-configuration test</code> instead of <code>switch</code>. <code>switch</code>
activates the configuration and updates the bootloader with a new entry for
this configuration. But in our case the VM doesn't have any bootloader so we
use <code>test</code> which just activate the new configuration.</p>
<p>So in order we can:</p>
<ol>
<li>
<p>Build the VM run script:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">NIX_PATH=nixpkgs=${HOME}&#x2F;vcs&#x2F;nixpkgs nix-build &#x27;&lt;nixpkgs&#x2F;nixos&gt;&#x27; -A vm --arg configuration .&#x2F;configuration.nix
</code></pre>
</li>
<li>
<p>Run the VM with some qemu options to have SSH access:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">QEMU_NET_OPTS=hostfwd=tcp::2221-:22 .&#x2F;result&#x2F;bin&#x2F;run-fripon-vm
</code></pre>
</li>
<li>
<p>Make some changes in the configuration</p>
</li>
<li>
<p>Deploy the new configuration with our deploy script</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh">.&#x2F;deploy.sh
these derivations will be built:
  &#x2F;nix&#x2F;store&#x2F;3r07vbxl1irc03xnca8xkcklx8329i2f-system-path.drv
  ...
building &#x27;&#x2F;nix&#x2F;store&#x2F;4zvybkbjdxxazsy2fmc2jzi3cyps63ms-nixos-system-fripon-19.09.git.3ad23e3.drv&#x27;...
Password:
copying 10 paths...
copying path &#x27;&#x2F;nix&#x2F;store&#x2F;ivlni1xcihrnf1hprfci3x88lwjyzvsh-system-path&#x27; to &#x27;ssh:&#x2F;&#x2F;root@localhost&#x27;...
...
Password:
Password:
activating the configuration...
setting up &#x2F;etc...
reloading user units for root...
setting up tmpfiles
reloading the following units: dbus.service
</code></pre>
</li>
</ol>
<p>Great! The new configuration was successfully built, uploaded to the VM and
then activated. Though we had to input the SSH password multiple times because no SSH
keys are setup.</p>
<h1 id="conclusion">Conclusion</h1>
<p>All the bits and pieces described here give us a really powerful way to iterate
on a NixOS configuration especially if you don't want to mess with a production
server.</p>
<p>Obviously all of this needs some lifting to make it more easy to use but
hopefully with this you can build something that works for you.</p>

</article>

      </main>
      <footer>
      

<nav>
    
    
    <a class="next" href="https:&#x2F;&#x2F;blog.patapon.info&#x2F;nixos-systemd-sway&#x2F;">Running Sway and friends with home-manager systemd user services ›</a>
    
</nav>


      </footer>
    </body>
</html>
