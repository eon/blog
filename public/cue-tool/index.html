<!DOCTYPE html>
<html lang="en">
    <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <title>Dejà vu - Introduction to the cue tooling layer</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="alternate" type="application/rss+xml" title="RSS" href="https://blog.patapon.info/rss.xml">
      
      <link rel="stylesheet" href="https://blog.patapon.info/site.css">
      
    </head>
    <body>
      <header>
        <h1><a href="https:&#x2F;&#x2F;blog.patapon.info">Dejà vu</a></h1>
        <nav>
        
          <a href="https:&#x2F;&#x2F;blog.patapon.info">Home</a>
           | 
        
          <a href="https:&#x2F;&#x2F;blog.patapon.info&#x2F;tags">Tags</a>
          
        
        </nav>
      </header>
      <main>
      
<article>
    
<h1>Introduction to the cue tooling layer</h1>
<p class="article-metadata">
  <small>
    <time>Posted on 2021-10-25</time> by eon@patapon.info<span class="tags">,
      Tags:
      
        <a href="https://blog.patapon.info/tags/cue/">#cue</a>
      
    </span>
    
  </small>
</p>

    <p>So you have used <code>cue</code> to create your configuration, but how to you actually do
something with it? Write a bash script to wrap calls to <code>cue</code>?</p>
<p>One special feature of <code>cue</code> compared to other configuration languages is
be ability to execute things from your configuration. After all, <code>cue</code> means
Configure, Unify, Execute and in this article we'll explore the execute part of
<code>cue</code>.</p>
<span id="continue-reading"></span>
<p>We call this execution part of <code>cue</code> the tooling layer or scripting layer.</p>
<p>This tooling layer gives to the user an easy way to actually do something with
the configuration directly in <code>cue</code> without relying on shell scripting. For
example you might want to write your configuration in a file and
run a cli tool with it or even apply <code>kubernetes</code> resource manifests you have
defined to a cluster.</p>
<p>Instead of wrapping the configuration in code or scripts, we wrap the code that
exploit the configuration inside the configuration itself (oh my that's meta!)
and use <code>cue</code> to run the code.</p>
<p>While a normal <code>cue</code> configuration evaluation is hermetic, the tooling layer
allows to interact with the outside world by allowing side effects such as
writing files or making http calls. Theses kind of side effects can only be
declared in <code>&lt;something&gt;_tool.cue</code> files and be run with the <code>cue cmd</code>
subcommand.  When using <code>cue eval</code> or <code>cue export</code> the tool files won't be
evaluated at all.</p>
<p>Let's dive in by defining a very simple <code>cue</code> command.</p>
<h2 id="defining-commands-and-tasks">Defining commands and tasks</h2>
<p><code>cue</code> will look for a <code>command</code> field at the root of <code>_tool.cue</code> files to
determine the list of available commands.</p>
<p>Let's create our first command in a file that ends with <code>_tool.cue</code>:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

import &quot;tool&#x2F;cli&quot;

command: &quot;hello-world&quot;: cli.Print &amp; {
    text: &quot;Hello!&quot;
}
</code></pre>
<p>We have defined an <code>hello-world</code> command which uses the task <code>cli.Print</code>. This
task simply output text on stdout.</p>
<p>Let's run it!</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh"> cue cmd hello-world
Hello!
# The command is also exposed directly as a subcommand of `cue`
 cue hello-world
Hello!
</code></pre>
<p>A command can contain multiple tasks and you can organize them as you want.
There is no particular structure to follow when defining tasks inside commands.
For example, this is fine:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

import &quot;tool&#x2F;cli&quot;

command: &quot;hello-world&quot;: {
    print: something: on: screen: cli.Print &amp; {
        text: &quot;Hello!&quot;
    }
    another: {
        task: cli.Print &amp; {
            text: &quot;Woot!&quot;
        }
    }
}
</code></pre>
<p>A task consists of a <code>cue</code> schema (<code>cli.Print</code> in this example) and some go
code builtin in the <code>cue</code> cli that will be executed when you run the command.</p>
<p>The definition of a task is just some data like your configuration. It is not a
function call or something like that. Because it is just data, you can use any
<code>cue</code> construct to generate or define tasks.</p>
<h3 id="task-types">Task types</h3>
<p>There aren't many builtin tasks provided by <code>cue</code> but the ones that are available
are pretty generic and allows you to derive specific tasks you might need in your
project.</p>
<p>The different tasks you can use in the tooling layer can be <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool">found
here</a>.</p>
<p>Currently you can find tasks to <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool/file">manipulate
files</a>, <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool/exec">execute
commands</a>, <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool/http">make HTTP
calls</a>.</p>
<h3 id="discovering-commands">Discovering commands</h3>
<p><code>cue help cmd</code> can list the different commands you have defined:</p>
<pre><code>[...]
Available Commands:
  hello-world
  oops
[...]
</code></pre>
<p>It is possible to provide some more &quot;special&quot; fields or a comment on the
command to improve the output:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">command: {
    &#x2F;&#x2F; Greeting command
    &#x2F;&#x2F;
    &#x2F;&#x2F; Greets you on the command line
    &quot;hello-world&quot;: cli.Print &amp; {
        text: &quot;Hello!&quot;
    }

    &quot;hello-world-2&quot;: cli.Print &amp; {
        $short: &quot;Gretting command&quot;
        $long:  &quot;Greets you on the command line&quot;
        text:   &quot;Hello!&quot;
    }
}
</code></pre>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh"> cue help cmd
[...]
Available Commands:
  hello-world   Greeting command
  hello-world-2 Gretting command
[...]
 cue help cmd hello-world
Greets you on the command line

Usage:
  cue cmd hello-world [flags]
[...]
</code></pre>
<h2 id="command-options">Command options</h2>
<p>In some cases a command might need some dynamic input provided by the user.
This could be an HTTP API endpoint URL, an optional flag to trigger some
specific tasks etc...</p>
<p>The <code>cue</code> tooling layer doesn't provide any specific feature for this but we can
use the generic injection mechanism of <code>cue</code> to handle this (<code>cue help injection</code>).</p>
<p>For example in our greeting command let's add field that can customize the
greeting output:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

import &quot;tool&#x2F;cli&quot;

who: *&quot;world&quot; | string @tag(who)

command: &quot;hello-world&quot;: cli.Print &amp; {
    text: &quot;Hello \(who)!&quot;
}
</code></pre>
<p>The field <code>who</code> has a default value of <code>world</code> but it can be changed using the
tag <code>who</code> on the cli:</p>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh"> cue cmd hello-world
Hello world!
 cue cmd -t who=cue hello-world
Hello cue!
</code></pre>
<p>With injection all <code>cue</code> unification constraint rules still applies.</p>
<p>Note: because of some parsing limitation the <code>-t k=v</code> option must be written
before the name of the command.</p>
<h2 id="handling-dependencies-between-tasks">Handling dependencies between tasks</h2>
<p>It is common to have multiple tasks that must be run in a specific order.</p>
<p>To handle this situation <code>cue</code> resolve dependencies between tasks much like
<code>terraform</code> does it between resources.</p>
<p>If a task references some field from another task, <code>cue</code> will automatically
treat this as a dependency:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

import (
    &quot;tool&#x2F;file&quot;
    &quot;tool&#x2F;cli&quot;
)

command: foo: {
    cmd1: file.Read &amp; {
        filename: &quot;file.txt&quot;
        contents: string
    }
    cmd2: cli.Print &amp; {
        text: cmd1.contents
    }
}
</code></pre>
<p>In this example we can see that <code>cmd2.text</code> field has a reference to
<code>cmd1.contents</code> field. Because of this <code>cmd2</code> will be run after <code>cmd1</code> has been
run successfully.</p>
<p>We can also see here a very powerful feature of <code>cue</code> tasks. The
<code>cmd1.contents</code> isn't concrete and will be resolved at runtime when the file is
actually read. The value will be filled by <code>cue</code> in the document and must respect
unification constraint rules. In this case the value must be a valid <code>string</code>.
Once <code>cmd1.contents</code> is filled with a concrete value the <code>cmd2</code> can be run.</p>
<p>Much like <code>terraform</code> <code>depends_on</code>, if a task needs to be run after some other
but doesn't reference any field of the other task you can simply
add a field that references the other task:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

import (
    &quot;tool&#x2F;file&quot;
    &quot;tool&#x2F;exec&quot;
)

command: foo: {
    cmd1: exec.Run &amp; {
        cmd: &quot;mkdir -p generated&quot;
    }
    cmd2: file.Create &amp; {
        $after: cmd1
        filename: &quot;.&#x2F;generated&#x2F;file.txt&quot;
        contents: &quot;foo&quot;
    }
}
</code></pre>
<p>In this example we introduce an <code>$after</code> field in <code>cmd2</code> that references
<code>cmd1</code> to materialize the dependency between the two tasks.</p>
<p>Since <code>file.Create</code> is not a definition you can define any field name you'd
like, it just needs to not clash with <code>file.Create</code> fields in this example.
<code>$after</code> has no particular meaning for the <code>cue</code> tooling layer.</p>
<h2 id="dynamic-tasks">Dynamic tasks</h2>
<p>Until now we defined tasks that didn't use any real configuration. Let's see
how tasks can refer to and use some configuration.</p>
<p>Imagine we manage a list of users in a <code>cue</code> configuration and we want to
provision them in some API.</p>
<p>First, we want first a clear schema of what a user is and define some:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package users

&#x2F;&#x2F; This is what a user look like
#User: {
	username:   string
	first_name: string
	last_name:  string
	email:      =~&quot;@example.com$&quot;
	role:       *&quot;developer&quot; | &quot;admin&quot;
}

users: [Username=string]: #User &amp; {username: Username}

&#x2F;&#x2F; Our list of users
users: {
	jdoe: {
		first_name: &quot;John&quot;
		last_name:  &quot;Doe&quot;
		email:      &quot;jdoe@example.com&quot;
		role:       &quot;admin&quot;
	}
	fday: {
		first_name: &quot;Francis&quot;
		last_name:  &quot;Day&quot;
		email:      &quot;francis@example.com&quot;
	}
}
</code></pre>
<p>This is a pretty simple <code>cue</code> configuration. We have a <code>#User</code> definition which constraints
the <code>users</code> struct values. Two users are defined.</p>
<p>Next we want to create these users in some API. We can create a <code>cue</code> command for this
by using the <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool/http"><code>tool/http</code> package</a>.
I'm using https://requestbin.com to post the data.</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package users

import (
  &quot;tool&#x2F;http&quot;
  &quot;encoding&#x2F;json&quot;
)

command: create: {
    for u in users {
        &#x2F;&#x2F; Generate a task for every user
        &quot;\(u.username)&quot;: http.Post &amp; {

            &#x2F;&#x2F; Our dummy API
            url: &quot;https:&#x2F;&#x2F;enelnux7735ki.x.pipedream.net&#x2F;users&quot;

            request: {
                header: &quot;Content-Type&quot;: &quot;application&#x2F;json&quot;
                &#x2F;&#x2F; Marshal the user info to JSON
                body: json.Marshal(u)
            }

            &#x2F;&#x2F; We expect a 200 HTTP code from the API
            &#x2F;&#x2F; Other codes will make the task fail.
            response: statusCode: 200
        }
    }
}
</code></pre>
<p>What's interesting here is that we use normal <code>cue</code> constructs to generate
the tasks (one per user). With a simple comprehension we can generate a task
for every user we need to provision.</p>
<p>We can also transform the data that is sent to the API. Here we marshal it
to JSON. But we could imagine also filtering out some fields or adding other
that would be required by the API.</p>
<p>And finally the success of the tasks is determined by unification. I've defined
<code>response.statusCode</code> to <code>200</code> so that if the API respond with some other code
the unification will fail because <code>cue</code> will try to unify <code>200</code> with some other
value and, thus, the task will fail.</p>
<h3 id="going-further">Going further</h3>
<p>With a classic REST API this probably won't work as once a user is created in the
system a POST request on an existing user will most likely trigger an HTTP 400
response.</p>
<p>Can <code>cue</code> handle this ? Well yes since <code>cue</code> is injecting HTTP calls response
data and status in the document it should be possible to specialize tasks based
on some field result, or even inject new tasks.</p>
<p>In our imaginary API, to determine if we need to create or update a user we
first need to get the user and then based on the status code proceed with the
appropriate HTTP call (POST or PUT).</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package users

import (
	&quot;tool&#x2F;http&quot;
	&quot;encoding&#x2F;json&quot;
)

users_api_base_url: &quot;https:&#x2F;&#x2F;my-user-api.example.com&#x2F;users&quot;

command: create: {
    for u in users {

        &#x2F;&#x2F; Get user from the API
        &quot;\(u.username)&quot;: http.Get &amp; {
            url: &quot;\(users_api_base_url)&#x2F;\(u.username)&quot;
            &#x2F;&#x2F; We handle only these status codes
            response: statusCode: 200 | 404
        }

        &#x2F;&#x2F; Common data for creating &#x2F; updating a user
        &quot;create_or_update_\(u.username)&quot;: http.Do &amp; {
            request: {
                header: &quot;Content-Type&quot;: &quot;application&#x2F;json&quot;
                body: json.Marshal(u)
            }
        }

        &#x2F;&#x2F; User doesn&#x27;t exists, do a POST on the users&#x2F; url
        if create[u.username].response.statusCode == 404 {
            &quot;create_or_update_\(u.username)&quot;: http.Post &amp; {
                url: users_api_base_url
            }
        }

        &#x2F;&#x2F; User exists, do a PUT on users&#x2F;username url
        if create[u.username].response.statusCode == 200 {
            &quot;create_or_update_\(u.username)&quot;: http.Put &amp; {
                url: &quot;\(users_api_base_url)&#x2F;\(u.username)&quot;
            }
        }

    }
}
</code></pre>
<p>It looks like we are doing imperative code but it's not. We're still defining
data and specializing the <code>create_or_update</code> task based on the result of the
<code>get</code> task.</p>
<p>As you can see, a lot can be done using the <code>cue</code> tooling layer, but don't get
too crazy!</p>
<h2 id="under-the-hood">Under the hood</h2>
<p>So we've seen that tasks are just data like any <code>cue</code> configuration but the difference
is that <code>cue</code> will run some code associated with each particular task. So how
does it know which code has to be run ?</p>
<p>If we look at <a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/pkg/tool/cli"><code>cli.Print</code> documentation</a>
we see:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">&#x2F;&#x2F; Print sends text to the stdout of the current process.
Print: {
	$id: *&quot;tool&#x2F;cli.Print&quot; | &quot;print&quot; &#x2F;&#x2F; for backwards compatibility

	&#x2F;&#x2F; text is the text to be printed.
	text: string
}
</code></pre>
<p>Every task as an <code>$id</code> field which is unique between tasks. When <code>cue</code> evaluates the command
it will walk all values and find all tasks denoted by this <code>$id</code> field. This is an
implementation detail that may change in the future.</p>
<p>The value of the <code>$id</code> field is used to know which go code has to be run for a
particular task.</p>
<p>Note also that tasks are not proper <code>cue</code> definitions as they should be. This
is for historic reasons because they were introduced in <code>cue</code> before definitions.</p>
<p>Let's give <code>cue</code> an unknown task to run and see what happens:</p>
<pre data-lang="cue" class="language-cue "><code class="language-cue" data-lang="cue">package foo

command: oops: {
    $id: &quot;tool&#x2F;cli.Oops&quot;
    text: &quot;Hello!&quot;
}
</code></pre>
<pre data-lang="sh" class="language-sh "><code class="language-sh" data-lang="sh"> cue cmd oops
runner of kind &quot;tool&#x2F;cli.Oops&quot; not found:
    .&#x2F;oops_tool.cue:3:10
</code></pre>
<p>Right, <code>cue</code> has found that this is a potential task but it has no go code registered
to run it so it exits with an error.</p>
<p>Currently there is no way to provide additional tasks to the <code>cue</code> cli but that
may be possible in the future.</p>
<h2 id="tooling-layer-caveats">Tooling layer caveats</h2>
<p>I noticed that if something is wrong in a <code>_tool.cue</code> file the error reporting
is not very good when using <code>cue cmd my-command</code>. In such cases I try to put
a maximum of code outside of the <code>_tool.cue</code> files (but no tasks obviously), and
debug the issue with <code>cue eval</code>.</p>
<p>In some cases the tasks dependencies are not properly discovered when using
comprehensions or guards, related issue:
<a href="https://github.com/cue-lang/cue/issues/1088">https://github.com/cue-lang/cue/issues/1088</a></p>
<p>You can't control error handling. If some task fails you cannot control what needs
to happen next, <code>cue</code> will stop the command right there and exit.</p>
<p>You can't run commands that are declared in imported modules/packages. This would be
a neat feature to allow package authors to distribute associated commands easily.</p>
<h2 id="conclusion">Conclusion</h2>
<p>In conclusion the <code>cue</code> tooling layer is just a way to describe side effects as
data to exploit your configuration.</p>
<p>Because tasks are just structured data you benefit of all <code>cue</code> constructs
and unification guarantees.</p>
<p>No need to export the data and importing it back in some script or other tool
to run actions, everything is contained and driven by the configuration itself!</p>
<p>In a next article we will explore the
<a href="https://pkg.go.dev/cuelang.org/go@v0.4.0/tools/flow"><code>tool/flow</code></a> API from the
<code>cue</code> go lib that is used by <code>cue cmd</code>.</p>

</article>

      </main>
      <footer>
      

<nav>
    
    <a class="previous" href="https:&#x2F;&#x2F;blog.patapon.info&#x2F;nix-run-direnv&#x2F;">‹ Direnv with nix run</a>
    
    
</nav>


      </footer>
    </body>
</html>
